<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="verification")
     */
    public function verification()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (in_array('ROLE_ADMIN', $currentuser->getRoles()) || in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            return $this->redirectToRoute('admin_dashboard');
        } else {
            return $this->redirectToRoute('dashboard');
        }
    }

    /**
     * @Route("/tableau_de_bord", name="dashboard")
     */
    public function indexAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $myVideo = $em->getRepository('AppBundle:AutorisedUser')->findBy([
            'user' => $currentuser
        ]);
        $recent=$em->getRepository('AppBundle:Video')->findBy(['status  '=>true],['id'=>'DESC'],3);
        return $this->render('default/index.html.twig', [
            'myVideo' => $myVideo,
            'recent' => $recent
        ]);
    }

    /**
     * @Route("/tableau_de_bord/admin", name="admin_dashboard")
     */
    public function indexAdminAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $myVideo = $em->getRepository('AppBundle:AutorisedUser')->findBy([
            'user' => $currentuser
        ]);

        return $this->render('default/adminView.html.twig', ['myVideo' => $myVideo]);
    }

    /**
     * @Route("/inscription", name="register")
     */
    public function registerAction(Request $request)
    {
        if ($request->getMethod() == 'POST') {
            $session = new Session();
            $username = $request->get('username');
            $email = $request->get('email');
            $telephone = $request->get('telephone');
            $role = $request->get('role');
            $pass = $request->get('pass');
            $confpass = $request->get('confpass');
            $em = $this->getDoctrine()->getManager();
            if ($pass != $confpass) {
                $message = "<b>The passwords are not the same!</b>";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('register');
            }
            $userManager = $this->get('fos_user.user_manager');
//            Create admin code


            // check if email doesn't exist
            $check = $userManager->findUserByEmail($email);
            if (empty($check)) {
                // save to fosusertable
                $user = $userManager->createUser();
                $user->setUsername($username);
                $user->setUsernameCanonical($username);
                $user->setEmail($email);
                $user->setEmailCanonical($email);
                $user->setEnabled(true);
                $user->addRole('ROLE_CLIENT');
//              this method will encrypt the password with the default settings :)
                $user->setPlainPassword($pass);
                $userManager->updateUser($user);
                $message = "<b>Bien enregistré, le client a été créé</b> ";
                $session->getFlashBag()->add('success', $message);

            } else {
                $message = "<b>Cette personne existe déjà !</b>";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('register');
            }
            return $this->redirectToRoute('register');

        }
        return $this->render('default/register.html.twig');
    }
}
