<?php
/**
 * Created by PhpStorm.
 * User: ProBook i5
 * Date: 10/10/2019
 * Time: 10:14 PM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class UserController extends Controller
{
    /**
     *
     * @Route("/new_user", name="new_user")
     */
    public function newUserAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $session = new Session();
        if (in_array('ROLE_ADMIN', $currentuser->getRoles()) || in_array('ROLE_CLIENT', $currentuser->getRoles()))       {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('dashboard');
        }

        if ($request->getMethod() === 'POST') {
            $username = $request->get('username');
            $email = $request->get('email');
            $telephone = $request->get('telephone');
            $role = $request->get('role');
            $pass = $request->get('pass');
            $confpass = $request->get('confpass');
            $em = $this->getDoctrine()->getManager();
            if ($pass != $confpass) {
                $message = "<b>Les mots de passe ne sont pas les mêmes!</b>";
                $this->get('session')->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('new_user');
            }
            $userManager = $this->get('fos_user.user_manager');
//            Create admin code


            // check if email doesn't exist
            $check = $userManager->findUserByEmail($email);
            if (empty($check)) {
                // save to fosusertable
                $user = $userManager->createUser();
                $user->setUsername($username);
                $user->setUsernameCanonical($username);
                $user->setEmail($email);
                $user->setEmailCanonical($email);
                $user->setEnabled(true);
                $user->addRole('ROLE_CLIENT');
//              this method will encrypt the password with the default settings :)
                $user->setPlainPassword($pass);
                $userManager->updateUser($user);
                $message = "<b>Bien enregistré, le client a été créé</b> ";
                $this->get('session')->getFlashBag()->add('success', $message);

            } else {
                $message = "<b>Cette personne existe déjà!</b>";
                $this->get('session')->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('new_user');
            }
            return $this->redirectToRoute('new_user');

        }

        return $this->render('users/newClient.html.twig');
    }

    /**
     * @Route("/client_list", name="client_list")
     */
    public
    function clientListeAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $session = new Session();
        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $message = "Vous n'avez pas accès à cette page";
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('dashboard');
        }
        $em = $this->getDoctrine()->getManager();
        $liste = $em->getRepository(User::class)->getlisteClient();
//        dump($liste);die();
        return $this->render('users/clientListe.html.twig', array(
            'liste' => $liste
        ));
    }

    /**
     * @Route("/admin_list", name="admin_list")
     */
    public
    function adminListeAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $session = new Session();
//        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
//            $message = "You dont have access to that page";
//            $this->get('session')->getFlashBag()->add('error', $message);
//            return $this->redirectToRoute('dashboard');
//        }
        $em = $this->getDoctrine()->getManager();
        $liste = $em->getRepository(User::class)->getlisteAdmin();
//        dump($liste);die();
        return $this->render('users/adminListe.html.twig', array(
            'liste' => $liste
        ));
    }

    /**
     *
     * @Route("/new_admin", name="new_admin")
     */
    public function newAdminAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
//        $session = new Session();
//        if (in_array('ROLE_ADMIN', $currentuser->getRoles()) || in_array('ROLE_CLIENT', $currentuser->getRoles()))       {
//            $message = "You dont have access to that page";
//            $this->get('session')->getFlashBag()->add('error', $message);
//            return $this->redirectToRoute('dashboard');
//        }

        if ($request->getMethod() === 'POST') {
            $username = $request->get('username');
            $email = $request->get('email');
            $telephone = $request->get('telephone');
            $role = $request->get('role');
            $pass = $request->get('pass');
            $confpass = $request->get('confpass');
            $em = $this->getDoctrine()->getManager();
            if ($pass != $confpass) {
                $message = "<b>Les mots de passe ne sont pas les mêmes !</b>";
                $this->get('session')->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('new_user');
            }
            $userManager = $this->get('fos_user.user_manager');
//            Create admin code


            // check if email doesn't exist
            $check = $userManager->findUserByEmail($email);
            if (empty($check)) {
                // save to fosusertable
                $user = $userManager->createUser();
                $user->setUsername($username);
                $user->setUsernameCanonical($username);
                $user->setEmail($email);
                $user->setEmailCanonical($email);
                $user->setEnabled(true);
                $user->addRole($role);
//              this method will encrypt the password with the default settings :)
                $user->setPlainPassword($pass);
                $userManager->updateUser($user);
                $message = "<b>Bien enregistré, le client a été créé</b> ";
                $this->get('session')->getFlashBag()->add('success', $message);

            } else {
                $message = "<b>Cette personne existe déjà!</b>";
                $this->get('session')->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('new_user');
            }
            return $this->redirectToRoute('new_user');

        }

        return $this->render('users/newAdmin.html.twig');
    }

    /**
     * @Route("/changeStat/{stat}/{id}",name="changeStat")
     */
    public function changeStatAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $currentuser = $this->getUser();
        $session = new Session();
        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $session->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        $stat = $request->get('stat');
        $id = $request->get('id');
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));
        // dump($user->getUsername());die();

        $user->setEnabled($stat);
        $userManager->updateUser($user);
        if ($stat == 1) {
            $message = "The account of <b>" . $user->getUsername() . "</b> has been activated";
        } else {
            $message = "The account of <b>" . $user->getUsername() . "</b> has been deactivated";
        }


        $session->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('admin_dashboard');
    }

    /**
     * @Route("/reset_passe/{id}",name="reset_passe")
     */
    public function reset_passeAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $id = $request->get('id');
        $session = new Session();
        $currentuser = $this->getUser();
        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $session->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('dashboard');
        }
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));
        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
//            Set new password
        $newpasse = 'password';
        $newpassword = $encoder->encodePassword($newpasse, $user->getSalt());
        $user->setPassword($newpassword);
        $userManager->updateUser($user);
        $message = "Vous venez de réinitialiser le mot de passe de " . $user->getUsername();
        $session->getFlashBag()->add('success', $message);
        if (in_array('ROLE_ADMIN', $user->getRoles()) || in_array('ROLE_SUPER_ADMIN', $user->getRoles())) {
            return $this->redirectToRoute('admin_dashboard');
        }
        return $this->redirectToRoute('admin_dashboard');
    }

}