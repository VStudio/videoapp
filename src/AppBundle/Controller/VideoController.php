<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AutorisedUser;
use AppBundle\Entity\User;
use AppBundle\Entity\Video;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\FileUploader;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Video controller.
 *
 * @Route("video")
 */
class VideoController extends Controller
{
    /**
     * Lists all video entities.
     *
     * @Route("/", name="video_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $videos = $em->getRepository('AppBundle:Video')->findAll();
        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $myVideo = $em->getRepository('AppBundle:AutorisedUser')->findBy([
                'user' => $currentuser
            ]);
//            dump($myVideo);die();
            return $this->render('video/clientview.html.twig', array(
                'videos' => $videos,
            ));
        }
        return $this->render('video/index.html.twig', array(
            'videos' => $videos,
        ));
    }

    /**
     * Creates a new video entity.
     *
     * @Route("/new", name="video_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();

        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('dashboard');
        }
        $video = new Video();
        $form = $this->createForm('AppBundle\Form\VideoType', $video);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted()) {
            $videoLink = $request->files->get('videoLink');
            $thumbnail = $request->files->get('thumbnail');
            $videoTarget = $this->getParameter("repertoire_videos");
            $thumbnailTarget = $this->getParameter("repertoire_thumb");
            $vid = $this->get('app.file_uploader')->uploadVideo($videoLink, $videoTarget);
            $thumbn = $this->get('app.file_uploader')->uploadImage($thumbnail, $thumbnailTarget);

            $video->setVideoLink($vid)
                ->setVideoExt($videoLink->getClientOriginalExtension())
                ->setThumbnail($thumbn);
            $em->persist($video);
            $em->flush();
            $message = "La vidéo a été créée avec succès";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('video_show', array('id' => $video->getId()));
        }
        return $this->render('video/new.html.twig', array(
            'video' => $video,
            'form' => $form->createView()

        ));
    }

    /**
     * Finds and displays a video entity.
     *
     * @Route("/{id}", name="video_show")
     * @Method("GET")
     */
    public function showAction(Video $video)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $check = $em->getRepository('AppBundle:AutorisedUser')->findBy([
                'video' => $video,
                'user' => $currentuser
            ]);
            if($check== null){
            $message = "Vous n'avez pas accès à cette vidéo, veuillez contacter l'admin";
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('dashboard');
            }

        }
        $assigeUser = $em->getRepository('AppBundle:AutorisedUser')->findBy([
            'video' => $video
        ]);
        $clientListe = $em->getRepository(User::class)->getlisteClient();
//dump($clientListe[0]);die();
        return $this->render('video/show.html.twig', array(
            'assigeUser' => $assigeUser,
            'video' => $video,
            'clientListe' => $clientListe

        ));
    }

    /**
     * Finds and displays a video entity.
     *
     * @Route("/assignUser", name="video_assign")
     * @Method("POST")
     */
    public function assignUserAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('dashboard');
        }
        $session = new Session();

        $em = $this->getDoctrine()->getManager();
        $users = $request->get('users');
        $videoId = $request->get('videoId');
        $video = $em->getRepository('AppBundle:Video')->find($videoId);
//        $userManager = $this->get('fos_user.user_manager');
        foreach ($users as $user) {
            $u = $em->getRepository('AppBundle:User')->find($user);

//            CHeck
            $check = $em->getRepository('AppBundle:AutorisedUser')->findBy([
                'video' => $videoId,
                'user' => $u
            ]);
            if ($check == null) {
                $ass = new  AutorisedUser();
                $ass->setUser($u)
                    ->setVideo($video);
                $em->persist($ass);
            }
        }
        $em->flush();
        $message = "Le ou les utilisateurs ont été assignés à cette vidéo avec succès";
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('video_show', array('id' => $videoId));
    }

    /**
     *
     * @Route("/unassign_user/{id}", name="unassign_user")
     * @Method("GET")
     */
    public function unassignUserAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('dashboard');
        }
        $session = new Session();
        $line = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $check = $em->getRepository('AppBundle:AutorisedUser')->find($line);
        $videoId= $check->getVideo()->getId();
        $em->remove($check);
        $em->flush();
        $message = "L'utilisateur a été désassigné de cette vidéo avec succès";
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('video_show', array('id' => $videoId));
    }

    /**
     * Displays a form to edit an existing video entity.
     *
     * @Route("/{id}/edit", name="video_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Video $video)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('dashboard');
        }
        $deleteForm = $this->createDeleteForm($video);
        $editForm = $this->createForm('AppBundle\Form\VideoType', $video);
        $editForm->handleRequest($request);
        $session = new Session();
        if ($editForm->isSubmitted()) {
//            $this->getDoctrine()->getManager()->flush();
            $videoLink = $request->files->get('videoLink');
            $thumbnail = $request->files->get('thumbnail');


            $em = $this->getDoctrine()->getManager();
            if (!empty($videoLink) && !empty($thumbnail)) {
                $videoTarget = $this->getParameter("repertoire_videos");
                $thumbnailTarget = $this->getParameter("repertoire_thumb");
                $vid = $this->get('app.file_uploader')->uploadVideo($videoLink, $videoTarget);
                $thumbn = $this->get('app.file_uploader')->uploadImage($thumbnail, $thumbnailTarget);
                $video->setVideoLink($vid)
                    ->setVideoExt($videoLink->getClientOriginalExtension())
                    ->setThumbnail($thumbn);
            }

            $em->persist($video);
            $em->flush();
            $message = "La vidéo a été modifiée avec succès";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('video_edit', array('id' => $video->getId()));
        }

        return $this->render('video/edit.html.twig', array(
            'video' => $video,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a video entity.
     *
     * @Route("/{id}", name="video_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Video $video)
    {
        $form = $this->createDeleteForm($video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($video);
            $em->flush();
        }

        return $this->redirectToRoute('video_index');
    }

    /**
     * Creates a form to delete a video entity.
     *
     * @param Video $video The video entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Video $video)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('video_delete', array('id' => $video->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
