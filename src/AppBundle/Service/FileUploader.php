<?php
namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    public function uploadImage(UploadedFile $file, $target)
    {
        if ($file->getClientOriginalExtension() != "jpg" && $file->getClientOriginalExtension() != "jpeg" && $file->getClientOriginalExtension() != "png" && $file->getClientOriginalExtension() != "JPG" && $file->getClientOriginalExtension() != "PNG"&& $file->getClientOriginalExtension() != "webp") {
            $fileName = "ne_pas_une image";
        } else {
            $fileName = md5(uniqid()) . '.' . $file->getClientOriginalExtension();
            $file->move($target, $fileName);
        }

        return $fileName;
    }
    public function uploadVideo(UploadedFile $file, $target)
    {
        if ($file->getClientOriginalExtension() != "avi" && $file->getClientOriginalExtension() != "mp4" && $file->getClientOriginalExtension() != "MP4" && $file->getClientOriginalExtension() != "AVI"&& $file->getClientOriginalExtension() != "webm"&& $file->getClientOriginalExtension() != "WEBM") {
            $fileName = "ne_pas_une image";
        } else {
            $fileName = md5(uniqid()) . '.' . $file->getClientOriginalExtension();
            $file->move($target, $fileName);
        }

        return $fileName;
    }
    public function uploadDoc(UploadedFile $file, $target,$title)
    {
        if ($file->getClientOriginalExtension() != "PDF" && $file->getClientOriginalExtension() != "pdf" && $file->getClientOriginalExtension() != "DOC" && $file->getClientOriginalExtension() != "DOX" && $file->getClientOriginalExtension() != "PPTX"&& $file->getClientOriginalExtension() != "doc"&& $file->getClientOriginalExtension() != "pptx") {
            $fileName = "ne_pas_une image";
        } else {
            $fileName = $title;
            $file->move($target, $fileName);
        }

        return $fileName;
    }

    public function uploadImageDoc(UploadedFile $file, $target)
    {
        if ($file->getClientOriginalExtension() != "jpg" && $file->getClientOriginalExtension() != "jpeg" && $file->getClientOriginalExtension() != "png" && $file->getClientOriginalExtension() != "JPG" && $file->getClientOriginalExtension() != "PNG"&& $file->getClientOriginalExtension() != "pdf" && $file->getClientOriginalExtension() != "PDF" && $file->getClientOriginalExtension() != "pdf" && $file->getClientOriginalExtension() != "DOC" && $file->getClientOriginalExtension() != "DOX" && $file->getClientOriginalExtension() != "PPTX"&& $file->getClientOriginalExtension() != "doc"&& $file->getClientOriginalExtension() != "pptx") {
            $fileName = "ne_pas_une image";
        } else {
            $fileName = md5(uniqid()) . '.' . $file->getClientOriginalExtension();
            $file->move($target, $fileName);
        }

        return $fileName;
    }
}
