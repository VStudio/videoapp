<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VideoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,array(
                'required' => true,
                'label' => 'Titre *',
            ))
            ->add('status', ChoiceType::class, array(
                'choices' => array(
                    'Publique' => '1',
                    'Priveé' => '0'
                ),
                'label' => 'Visibilite de la video *',
                'required' => true,
                'choices_as_values' => true
            ))
            ->add('description', TextareaType::class, array(
                'required' => false,
                'label' => 'Description *',
            ))
//            ->add('videoLink')
//            ->add('thumbnail')
            ->add('category', EntityType::class, array(
                'class' => 'AppBundle\Entity\Categories',
                'choice_label' => 'title',
                'required' => true,
                'placeholder' => 'Choisissez une catégorie','label' => 'Catégorie *'));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Video'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_video';
    }


}
