var ajax;
ajax = {
    // Verify sponsor code
    checkCode: function () {
        var sponsorcode = $('#sponsorcode').val();
        // console.log(sponsorcode);
        $('#checkintxt').css('display', 'block');
        $('#defaultText').css('display', 'none');

        $.ajax({
            type: "POST",
            url: Routing.generate('checkSponorCode'),
            data: {
                sponsorcode: sponsorcode
            },
            success: function (data) {
                $('#checkintxt').css('display', 'none');
                $('#defaultText').html(data).css('color', 'green').fadeIn(1000);
            }
        });
    },
    checkTransferCode: function () {
        var sponsorcode = $('#sponsorcode').val();
        // console.log(sponsorcode);
        $('#checkintxt').css('display', 'block');
        $('#defaultText').html('Verifying code...');

        $.ajax({
            type: "POST",
            url: Routing.generate('checkSponorCode'),
            data: {
                sponsorcode: sponsorcode
            },
            success: function (data) {
                $('#checkintxt').css('display', 'none');
                $('#defaultText').html(data).fadeIn(1000);
            }
        });
    },
    checkTransferCode2: function () {
        var sponsorcode = $('#placementcode').val();
        // console.log(sponsorcode);
        $('#checkintxt').css('display', 'block');
        $('#defaultText2').html('Verifying code...');

        $.ajax({
            type: "POST",
            url: Routing.generate('checkSponorCode'),
            data: {
                sponsorcode: sponsorcode
            },
            success: function (data) {
                $('#checkintxt').css('display', 'none');
                $('#defaultText2').html(data).fadeIn(1000);
            }
        });
    },
    checkDistributorCode: function () {
        var sponsorcode = $('#appbundle_productorder_distributor').val();
        $('#defaultText').html('Verifying code...');

        $.ajax({
            type: "POST",
            url: Routing.generate('checkSponorCode'),
            data: {
                sponsorcode: sponsorcode
            },
            success: function (data) {
                $('#defaultText').html(data).fadeIn(1000);
            }
        });
    },
    checkProduct: function () {
        var product = $('#appbundle_productorder_product').val();
        $('#productDetail').html('Please wait...');

        $.ajax({
            type: "POST",
            url: Routing.generate('checkProduct'),
            data: {
                product: product
            },
            success: function (data) {
                $('#productDetail').html(data).fadeIn(1000);
            }
        });
    },
    getPriceProduct: function () {
        var product = $('#appbundle_productorder_product').val();
// console.log(product);
$.ajax({
    type: "POST",
    url: Routing.generate('getPriceProduct'),
    data: {
        product: product
    },
    success: function (data) {
        console.log(data);
        $('#price').val(data[0]);
        $('#pv').val(data[1]);
        
        var quantity = $('#appbundle_productorder_quantite').val();
        var prix = $("#price").val();
        var montant = quantity * data[0];
        $("#totalprice").val(montant);
        var pv = $("#pv").val();
        var montantPv=  quantity * data[1];
        $("#totalpv").val(montantPv);
    }
});
},
checkMessageCode: function () {
    var sponsorcode = $('#recievercode').val();
        // console.log(sponsorcode);
        $('#checkintxt').css('display', 'block');
        $('#defaultSms').html('Verifying code...');

        $.ajax({
            type: "POST",
            url: Routing.generate('checkSponorCode'),
            data: {
                sponsorcode: sponsorcode
            },
            success: function (data) {
                $('#checkintxt').css('display', 'none');
                $('#defaultSms').html(data).fadeIn(1000);
            }
        });
    },
    getlevelcommission: function () {
        $('#commission').html('Calculating ...');
        // var productId = $(this).next().html('level');
        $.ajax({
            type: "POST",
            url: Routing.generate('calcCommission'),
            data: {},
            success: function (data) {

                $('#commission').delay(1200).html(data).fadeIn(1000);
            }
        });

    },
    downlinetree: function (user) {
        var loader = 'Loading please wait <i class="fas fa-sync-alt fa-spin"></i>';
        var id = '#' + user;
        var name = $('.clientname').text();
        // console.log(name,name.length);
        $(id).html(loader);
        $.ajax({
            type: "POST",
            url: Routing.generate('getBranches'),
            data: {
                user: user
            },
            success: function (data) {

                $(id).delay(1200).html(data).fadeIn(1000);
                // $('.box-username').each(function(obj) {
                //     // var objetctext= $(this).text();
                //     // if(objetctext.length > 15){
                //         var substring= objetctext.substr(0, 13);
                //         console.log(objetctext);
                //         $(this).text(substring+'..');
                //     // }
                // });
            }
        });

    },
    sponsortree: function (user) {
        var loader = 'Loading please wait <i class="fas fa-sync-alt fa-spin"></i>';
        var id = '#' + user;
        var name = $('.clientname').text();
        // console.log(name,name.length);
        $(id).html(loader);
        $.ajax({
            type: "POST",
            url: Routing.generate('getPlacementBranches'),
            data: {
                user: user
            },
            success: function (data) {

                $(id).delay(1200).html(data).fadeIn(1000);
                // $('.box-username').each(function(obj) {
                //     // var objetctext= $(this).text();
                //     // if(objetctext.length > 15){
                //         var substring= objetctext.substr(0, 13);
                //         console.log(objetctext);
                //         $(this).text(substring+'..');
                //     // }
                // });
            }
        });

    },
    newmessagechat: function () {
        var sendercode = $('#senderuser').val();
        var recievercode = $('#recieveruser').val();
        var newmessage = $('#newmessagefield').val();
        var sentby = $('#sentby').val();

        $.ajax({
            type: "POST",
            url: Routing.generate('createnewchatmsg'),
            data: {
                sendercode: sendercode,
                recievercode: recievercode,
                sentby: sentby,
                newmessage: newmessage
            },
            success: function (data) {
                $('#refreshmsg').trigger('click');
                $('#closenewmessage2').trigger('click');
                if (data === 'message sucess') {
                    $('#sucessreponse').trigger('click');
                } else if (data === 'user false') {
                    $('#dangerreponse').trigger('click');
                }

                $('#recievercode').val('');
                $('#newmessage').val('');
            }
        });
    },
    newmessage: function () {
        var recievercode = $('#recievercode').val();
        var newmessage = $('#newmessage').val();

        $.ajax({
            type: "POST",
            url: Routing.generate('createnewchat'),
            data: {
                recievercode: recievercode,
                newmessage: newmessage
            },
            success: function (data) {
                $('#refreshmsg').trigger('click');
                $('#closenewmessage').trigger('click');
                if (data === 'message sucess') {
                    $('#sucessreponse').trigger('click');
                } else if (data === 'user false') {
                    $('#dangerreponse').trigger('click');
                }

                $('#recievercode').val('');
                $('#newmessage').val('');
            }
        });
    },
    refreshmsglist: function () {
        $.ajax({
            type: "POST",
            url: Routing.generate('refreshMsg'),
            data: {},
            success: function (data) {
                $('#messagelist').html(data);
                var numItems = $('.list-group-item').length;
                if (numItems > 5) {
                    $('#loadmore').show();
                } else {
                    $('#loadmore').hide();
                }
            }
        });
    },
    loadmoremsglist: function () {
        var page = $('#pagenumber').val();
        $.ajax({
            type: "POST",
            url: Routing.generate('loadMoreMsg'),
            data: {
                page: page
            },
            success: function (data) {
                var p = parseInt(page) + 1;
                $('#pagenumber').val(p);
                $('#messagelist').append(data);
            }
        });
    },
    getchatlist: function (msgid) {
        var msgid = msgid;
        $('this').removeClass('custom-dark-bg');
        $.ajax({
            type: "POST",
            url: Routing.generate('getchatlist'),
            data: {
                msgid: msgid
            },
            success: function (data) {
                $('#modal-content').html(data);
            }
        });
    },
    notifypush: function () {
        $.ajax({
            type: "POST",
            url: Routing.generate('notificationscount'),
            data: {
                msgid: msgid
            },
            success: function (data) {
                $('#modal-content').html(data);
            }
        });
    },
    fliterTransaction: function () {
        var period = $('#date-picker-example').val();
        $('#hiddenText').show();
        // console.log(period);
        $.ajax({
            type: "POST",
            url: Routing.generate('transaction_periode'),
            data: {
                period: period
            },
            success: function (data) {
                $('#hiddenText').hide();
                $('#filtered').html(data);
            }
        });
    },

    saveEditable: function () {
        var type = $('#type').val();
        var editableField = $('#editableField').html();
        // var productId = $(this).next().html('level');
        $.ajax({
            type: "POST",
            url: Routing.generate('saveEditable'),
            data: {
                type: type,
                editableField: editableField
            },
            success: function (data) {

                // editableField
                            }
        });

    },
}