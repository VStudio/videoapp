//show password
var ckbox = $('#displayPassword');

ckbox.mouseover(function () {
	$('#pwd1').attr('type', 'text');
});
ckbox.mouseleave(function () {
	$('#pwd1').attr('type', 'password');
});
var ckboxconf = $('#checkboxconf');
$(ckboxconf).on('click', function () {
	if (ckboxconf.is(':checked')) {
		$('#passwordFconf').attr('type', 'text');
	} else {
		$('#passwordFconf').attr('type', 'password');
	}
});

// Check if password is correct
function checkPasswordMatch() {

	var password = $("#password").val();
	var confirmPassword = $("#conf-pass").val();
	if (password !== confirmPassword) {
		$("#divCheckPasswordMatch").removeClass('hidden');
		$("#divCheckPasswordMatch").html("<span style='color: firebrick;font-weight: bold;font-size: 16px;' class='mb-3'><i class='fa fa-times'></i> The passwords entered do not match!</span><br>");
		$('#send').attr('disabled', 'disabled');
	}
	else if (password === '') {
		$("#divCheckPasswordMatch").addClass('hidden');
	}
	else {
		$("#divCheckPasswordMatch").removeClass('hidden');

		$("#divCheckPasswordMatch").html("<span style='color: green;font-weight: bold;font-size: 16px;' class='mb-3'><i class='fa fa-check'></i> The password entered are a match</span>");
	}
}

// $("#conf-pass").keyup();
